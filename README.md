Stopki wiadomości email dla Billboard-x

Sposób użycia:

- wybrać plik html odpowiadający imieniu i nazwisku pracownika
- skopiować 100% zawartości do schowka lub zapisać plik na dysku konputera
- skorzystać z ustawień programu pocztowego służących do wskazania/wklejenia treści stopki
- po konfiguracji programu pocztowego stopka powinna dołączać się do wiadomości automatycznie.

Dodawanie nowych stopek:
- pobrać repozytorium
- zduplikować istniejącą stopkę i  odpowiednio ją nazwać
- dodkonać niezbędnych edycij treści
- zakomitować zmiany do repo
- stworzyć merge requesta do głównej galężi i go zatwierdzić
- po wykonaniu tych czynności nowa stopka będzie dostępna do pobrania